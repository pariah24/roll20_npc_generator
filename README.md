# README #

This is a custom API script for generating D&D 5e monster sheets for Roll20.

USAGE:
To generate a monster, type the command !npc_gen into roll20 followed by a list of a monsters you wish to generate. i.e !npc_gen "Dire Wolf" "Giant Lizard"

NOTE:
The Roll20 API does not support contacting external files for doing data lookups. As such, the monster data is built into the script itself which is why it is so large.